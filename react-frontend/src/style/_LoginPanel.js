import styled from "styled-components";

export default {
  DivLoginStyle: styled.div`
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  `,

  DivPanelStyle: styled.div`
    display: flex;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
  `,

  DivLogoStyle: styled.div`
    border: 0;
    border-radius: 1rem;
    box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
    margin-bottom: 3rem !important;
    margin-top: 3rem !important;
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #f39c12;
    background-clip: border-box;
  `,

  DivLogoContainerStyle: styled.div`
    display: grid;
    justify-content: center;
    padding: 2rem;
    padding-top: 6em;
  `,

  ImgStyle: styled.img`
    height: 150px;
    border-radius: 50%;
    border: 2px solid white;
    background-color: #4b0204;
  `,

  FormMainStyle: styled.form`
    width: 100%;
  `,

  DivGroupStyle: styled.div`
    position: absolute;
    top: -90px;
    display: grid;
    justify-content: center;
    width: 90%;
  `,

  DivCardStyle: styled.div`
    background: antiquewhite;
    padding: 10px;
    justify-content: center !important;
    border-radius: 50%;
  `
};
