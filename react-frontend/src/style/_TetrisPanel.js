import styled from "styled-components";

export default {
  DivPanelStyle: styled.div`
    display: grid;
    grid-template-columns: repeat(6, 1fr);
    background-color: #38000591;
    padding: 8px;
    border: 2px solid #494536;
  `,

  DivBoardStyle: styled.div`
    display: grid;
    justify-items: center;
    align-content: center;
    border: 2px solid #000;
    padding: 3px;
  `,

  SpanStyle: styled.span`
    font-size: small !important;
  `,

  DivTetriStyle: styled.div`
    display: grid;
    min-width: 280px;
    justify-content: center;
    font-size: x-small;
    text-align: center;
  `,

  DivTetriMainStyle: styled.div`
    display: grid;
    min-width: 340px;
    justify-content: center;
    background-color: #9ead86;
    text-align: center;
  `,

  DivBoardTopStyle: styled.div`
    height: 95vh;
  `,

  TdQueueStyle: styled.td`
    width: 20px;
    height: 20px;
  `,

  TdDisablesStyle: styled.td`
    width: 20px;
    height: 20px;
    border: 2px solid #879372;

    &::after {
      background: #879372;
    }
  `
};
