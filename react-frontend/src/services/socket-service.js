import Constants from "../utils/Constants";
import EventEmitter from "../utils/event-emitter";

/**
 * @author Jônathas Assunção
 * @contact  jaa020399@gmail.com
 * @date 30/05/2019
 */
const { url, events } = Constants;
let socket = new WebSocket(url.SOCKET);
function emitEvent(data) {
  console.log(`treating the message: '${data}'`);
  const command = data.split(" ");
  const idPlayer = Number(command[1]);

  switch (command[0]) {
    case "f":
      const gameboard = data.substring(data.indexOf(command[2]));
      socketService().event.emit(events.GAME, idPlayer, gameboard);
      break;

    case "fp":
      const preview = data.substring(data.indexOf(command[2]));
      socketService().event.emit(events.PREVIEW, idPlayer, preview);
      break;

    case "fs":
      const specialQueue = data.substring(data.indexOf(command[2]));
      socketService().event.emit(events.QUEUE, idPlayer, specialQueue);
      break;

    case "pline":
      const message = data.substring(data.indexOf(command[2]));
      socketService().event.emit(events.CHAT, idPlayer, message);
      break;

    case "startgame":
      const statusGame = command[1];
      socketService().event.emit(events.STARTGAME, Boolean(+statusGame));
      break;

    case "pause":
      const statusPause = command[1];
      socketService().event.emit(events.PAUSE, Boolean(+statusPause));
      break;

    case "playerleave":
      socketService().event.emit(events.LEAVE, idPlayer);
      break;

    case "playerlost":
      socketService().event.emit(events.LOST, idPlayer);
      break;

    case "playerwon":
      socketService().event.emit(events.WON, idPlayer);
      break;

    case "team":
      const teamPlayer = command[2];
      socketService().event.emit(events.TEAM, idPlayer, teamPlayer);
      break;

    case "playerjoin":
      const nickname = command[2];
      socketService().event.emit(events.JOIN, idPlayer, nickname);
      break;

    default:
      console.log(`The client could not handle the message: '${data}'`);
      break;
  }
}

function socketService() {
  return {
    startSocket: () => {
      console.log(`try connect: ${url.SOCKET}`);
      socket.onopen = () => {
        console.log(`connected WS: ${url.SOCKET}`);
        socket.onmessage = ({ data }) => {
          console.log(`Receiving message: '${data}' to ${url.SOCKET}`);
          emitEvent(data);
        };
      };
    },

    closeSocket: () => {
      socket.onclose = () => {
        console.log("disconnected WS");
        console.log(`try reconnect: ${url.SOCKET}`);
        socket = new WebSocket(url.SOCKET);
      };
    },

    sendMSG: msg => {
      console.log(`send message: '${msg}' to ${url.SOCKET}`);
      socket.send(msg);
    },
    event: EventEmitter
  };
}

export default socketService;
