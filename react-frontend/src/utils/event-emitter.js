import { EventEmitter } from "events";
import _ from "lodash";


const EE = _.extend(
  {
    emit(NAME, args) {
      this.emit(NAME, args);
    },

    addListener(NAME, callback) {
      this.on(NAME, callback);
    },

    removeListener(NAME, callback) {
      this.removeListener(NAME, callback);
    }
  },
  EventEmitter.prototype
);

export default EE;
