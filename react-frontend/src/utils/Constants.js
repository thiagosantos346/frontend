/**
 * @update Jônathas Assunção
 * @contact  jaa020399@gmail.com
 * @date 28/05/2019
 */

export default {
  actions: {
    HARD_DROP: "HARD_DROP",
    MOVE_DOWN: "MOVE_DOWN",
    MOVE_LEFT: "MOVE_LEFT",
    MOVE_RIGHT: "MOVE_RIGHT",
    PAUSE: "PAUSE",
    FLIP_CLOCKWISE: "FLIP_CLOCKWISE",
    FLIP_COUNTERCLOCKWISE: "FLIP_COUNTERCLOCKWISE",
    RESUME: "RESUME",
    DROP_SPECIAL: "DROP_SPECIAL"
  },

  dimension: {
    GAME_WIDTH: 22,
    GAME_HEIGHT: 12,
    MAX_PLAYERS: 6,
    QUEUE_PREVIEW: 4,
    QUEUE_SPECIAL: 10
  },

  events: {
    LOGIN: "LOGIN",
    GAME: "GAME",
    CHAT: "CHAT",
    QUEUE: "QUEUE",
    PREVIEW: "PREVIEW",
    STARTGAME: "STARTGAME",
    PAUSE: "PAUSE",
    LEAVE: "LEAVE",
    JOIN: "JOIN",
    WON: "WON",
    LOST: "LOST",
    SPECIAL: "SPECIAL"
  },

  url: {
    REST: "https://viacep.com.br/ws",
    SOCKET: "ws://localhost:3030"
  }
};
