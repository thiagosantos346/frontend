import React, { Component } from "react";
import restService from "../services/rest-service";
import _TetrisPanel from "../style/_TetrisPanel";
import Constants from "../utils/Constants";
import EventEmitter from "../utils/event-emitter";

const { DivTetriMainStyle, DivTetriStyle, TdQueueStyle } = _TetrisPanel;
const { dimension, events } = Constants;

class PieceQueueComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: props.player,
      board: null,
      gamePanel: []
    };
  }

  componentDidMount() {
    this.updateBoard(this.startBoard());

    EventEmitter.addListener(events.PREVIEW, (id, value) => {
      if (restService().getPlayer(this.state.player).id === id) {
        this.updateBoard(value);
      }
    });
  }

  startBoard() {
    // const total = dimension.QUEUE_PREVIEW * dimension.QUEUE_PREVIEW;
    // let boardClear = "";

    // for (var i = 0; i < total; i++) boardClear += "0";

    // return boardClear;
    return "0300030003300000";
  }

  updateBoard(board) {
    this.setState({ board }, () => {
      this.converterString(this.state.board);
    });
  }

  converterString(string) {
    const gamePanel = [];

    for (var i = 0; i < string.length; i += dimension.QUEUE_PREVIEW) {
      const row = string.substr(i, dimension.QUEUE_PREVIEW);
      const rowArray = [];
      for (var y = 0; y < dimension.QUEUE_PREVIEW; y++) {
        const classString = ` game-block piece-${row.charAt(y)}`;
        rowArray.push(<TdQueueStyle key={y} className={classString} />);
      }
      gamePanel.push(<tr key={i}>{rowArray}</tr>);
    }

    this.setState({ gamePanel });
  }

  render() {
    return (
      <DivTetriMainStyle>
        <DivTetriStyle>
          <table>
            <tbody>{this.state.gamePanel}</tbody>
          </table>
        </DivTetriStyle>
      </DivTetriMainStyle>
    );
  }
}

export default PieceQueueComponent;
