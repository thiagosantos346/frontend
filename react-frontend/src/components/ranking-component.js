import React, { Component } from "react";
import restService from "../services/rest-service";

class RankingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { list: [] };
    this.update = this.update.bind(this);
  }

  update() {
    restService()
      .getRanking()
      .then(res => {
        console.log("Success in obtaining the ranking resource");
        let result = [
          { type: "J", nick: "Player 2", points: 500 },
          { type: "T", nick: "Team 3", points: 200 }
        ];
        this.setState({
          list: this.listRanking(result)
        });
      })
      .catch(err => {
        console.log("Unsuccess in obtaining the ranking resource");
      });
  }

  listRanking(list) {
    return (
      <ul className="list-group">
        {list.map((el, i) => {
          const type = el.type === "J" ? "Jogador" : "Team";
          return (
            <li
              key={i}
              className="list-group-item d-flex justify-content-between align-items-center"
            >
              {type} <b>{el.nick}</b>
              <span className="badge badge-primary badge-pill">
                {el.points} pts
              </span>
            </li>
          );
        })}
      </ul>
    );
  }

  render() {
    return (
      <div className="card border-primary ">
        <div className="card-header">
          Ranking
          <button
            type="button"
            className="badge badge-primary"
            onClick={this.update}
          >
            Atualizar
          </button>
        </div>
        <div className="card-body text-primary">{this.state.list}</div>
      </div>
    );
  }
}

export default RankingComponent;
