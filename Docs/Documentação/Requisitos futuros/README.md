**Requisitos futuros**  

Este documento vista listar as principais funcionalidades que não serão cobertas no projeto, afim de deixar o usuário informado.

Os requisitos futuros diz respeito aquelas funcionalidades que o cliente não terá disponibilizado no atual projeto. Ao longo do projeto foram detectadas algumas funcionalidades que seriam de alta dificuldade e/ou fugiam do foco do projeto proposto.

 - **1. No chat permitir áudio** 	 Nesta versão o jogo disponibiliza um chat, mas limitado ao tipo textual. Não permite o envio de áudio,
   para a interação realista dos jogadores.
   
 
 - **2. No chat permitir vídeo:** Nesta versão o jogo disponibiliza um chat, mas limitado ao tipo textual. Não permite o envio de vídeo,
   para a interação realista dos jogadores.
   
 - **3. No chat permitir compartilhamento de foto** Nesta versão o jogo disponibiliza um chat, mas limitado ao tipo textual. Não permite o envio de imagem, para a interação realista dos jogadores.
 
 - **4. Personalização do ambiente do jogo** 	Nesta versão do jogo o jogador não tem a opção de personalizar o ambiente de jogo, como por exemplo: adicionar novos sons, mudar cores de alguns campos, escolher um avatar, e etc.



