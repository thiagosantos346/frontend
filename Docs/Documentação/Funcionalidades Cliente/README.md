# Principais Funcionalidades

## Iniciar Jogo

- Clicar em 'Iniciar Jogo'
> ``` startgame 1 5 ```
* Criando Evento e emitindo evento na conexão
```javascript
updateStart() {
const  msg  =  `startgame ${Number(!this.state.startGame)}  ${
restService().getPlayer(this.state.player).id
}`;
socketService().sendMSG(msg);
}
```
```javascript
case  "startgame":
const  statusGame  =  command[1];
socketService().event.emit(events.STARTGAME, Boolean(+statusGame));
break;
```

##  Pausar Jogo

- Clicar em 'Pausar Jogo'
> ```pause 1 5```
* Criando Evento e emitindo evento na conexão
```javascript
updatePause() {
const  msg  =  `pause ${Number(!this.state.pauseGame)}  ${
restService().getPlayer(this.state.player).id
}`;
socketService().sendMSG(msg);
}
```
```javascript
case  "pause":
const  statusPause  =  command[1];
socketService().event.emit(events.PAUSE, Boolean(+statusPause));
break;
```

## Finalizar Jogo

- Clicar em 'Finalizar Jogo'
> ``` startgame 0 5 ```
* Criando Evento e emitindo evento na conexão
```javascript
logoff() {
const  msg  =  `playerleave
${restService().getPlayer(this.state.player).id}`;
socketService().sendMSG(msg);
}
```
```javascript
case  "startgame":
const  statusGame  =  command[1];
socketService().event.emit(events.STARTGAME, Boolean(+statusGame));
break;
```

## Mover peça

### Direita

- Seta para direita
> ``` k 5 right```
* Criando Evento e emitindo evento na conexão
```javascript
function  generateBoard(shortcut) {
socketService().sendMSG(`k ${player.id}  ${shortcut}`);
}
```
```javascript
sendMSG:  msg  => {
console.log(`send message: '${msg}' to ${url.SOCKET}`);
socket.send(msg);
}
```

### Esquerda

- Seta para esquerda
> ```k 5 left```
* Criando Evento e emitindo evento na conexão
```javascript
function  generateBoard(shortcut) {
socketService().sendMSG(`k ${player.id}  ${shortcut}`);
}
```
```javascript
sendMSG:  msg  => {
console.log(`send message: '${msg}' to ${url.SOCKET}`);
socket.send(msg);
}
```

### Girar

- Seta para cima
> ```k 5 up```
* Criando Evento e emitindo evento na conexão
```javascript
function  generateBoard(shortcut) {
socketService().sendMSG(`k ${player.id}  ${shortcut}`);
}
```
```javascript
sendMSG:  msg  => {
console.log(`send message: '${msg}' to ${url.SOCKET}`);
socket.send(msg);
}
```

### Baixar

- Seta para baixo
> ```k 5 down```

```javascript
function  generateBoard(shortcut) {
socketService().sendMSG(`k ${player.id}  ${shortcut}`);
}
```
```javascript
sendMSG:  msg  => {
console.log(`send message: '${msg}' to ${url.SOCKET}`);
socket.send(msg);
}
```

## Chat

- Enviando uma mensagem de texto no chat.
> ```pline 5 enviando mensagem```
```javascript
case  "pline":
const  message  =  data.substring(data.indexOf(command[2]));
socketService().event.emit(events.CHAT, idPlayer, message);
break;
```

## Ranking

* Clicar no botão ranking 
```javascript
update() {
restService()
.getRanking()
.then(res  => {
console.log("Success in obtaining the ranking resource");
let  result  = [
{ type:  "J", nick:  "Player 2", points:  500 },
{ type:  "T", nick:  "Team 3", points:  200 }
];
this.setState({
list:  this.listRanking(result)
});
})
.catch(err  => {
console.log("Unsuccess in obtaining the ranking resource");
});
}
```

## Especiais

- Aplicando uma especial 
- Pressionando tecla: "1", "2", "3", "4", "5", "6";
- Exemplo: Adicionar uma linha: 
> ```k 5 a```
* Criando Evento e emitindo evento na conexão
```javascript
function  generateBoard(shortcut) {
socketService().sendMSG(`k ${player.id}  ${shortcut}`);
}
```
```javascript
sendMSG:  msg  => {
console.log(`send message: '${msg}' to ${url.SOCKET}`);
socket.send(msg);
}
```
- Especiais obtidos pelos jogador
> ```fs 4 bnooacqnbn```
```javascript
* Criando Evento e emitindo evento na conexão
function  generateBoard(shortcut) {
socketService().sendMSG(`fs ${player.id}  ${genSpecial()}`);
}
```
```javascript
sendMSG:  msg  => {
console.log(`send message: '${msg}' to ${url.SOCKET}`);
socket.send(msg);
}
```
## Preview 

- Visualizando próximas de peças
> ```fp 1 7799553977299887```
* Criando Evento e emitindo evento na conexão
```javascript
function  generateBoard(shortcut) {
socketService().sendMSG(`fp ${player.id}  ${genPreview()}`);
}
```
```javascript
sendMSG:  msg  => {
console.log(`send message: '${msg}' to ${url.SOCKET}`);
socket.send(msg);
}





