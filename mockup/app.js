const WebSocket = require("ws");

const wss = new WebSocket.Server({ port: 3030 });

wss.on("connection", function connection(ws) {
  ws.on("message", function incoming(data) {
    console.log("msg:", data);
    wss.clients.forEach(function each(client) {
      client.send(data);
      // if (client !== ws && client.readyState === WebSocket.OPEN) { //Não responde a mensagem para o cliente que enviou
      //   client.send(data);
      // }
    });
  });
});
